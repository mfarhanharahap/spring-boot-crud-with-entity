package com.example.springdatajpa.services;

import com.example.springdatajpa.entity.Order;
import com.example.springdatajpa.repository.OrderRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServices {

    @Autowired
    OrderRespository orderRespository;

    public List<Order> getOrderByUserId(List<Long> user_id) {
        List<Order> listOrder = new ArrayList<>();
        orderRespository.findAllById(user_id).forEach(order -> {listOrder.add(order);});
        return listOrder;
    }

    public List<Order> getAllOrders() {
        List<Order> listOrder = new ArrayList<>();
        orderRespository.findAll().forEach(order -> {listOrder.add(order);});
        return listOrder;
    }

    public void createOrder(Order order) {
        orderRespository.save(order);
    }

    public void deleteOrderById(Long id) {
        orderRespository.deleteById(id);
    }

    public void updateOrderById(Order orders, Long id) {
        orderRespository.findById(id).map(order -> {
            order.setUser_id(orders.getUser_id());
            order.setName_product(orders.getName_product());
            order.setPrice(orders.getPrice());
            order.setQuantity(orders.getQuantity());
            return orderRespository.save(order);
        }).orElseGet(() -> {return orderRespository.save(orders);});
    }
}
