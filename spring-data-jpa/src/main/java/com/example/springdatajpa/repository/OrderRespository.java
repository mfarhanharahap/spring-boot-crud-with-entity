package com.example.springdatajpa.repository;

import com.example.springdatajpa.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRespository extends CrudRepository<Order, Long> {
}
