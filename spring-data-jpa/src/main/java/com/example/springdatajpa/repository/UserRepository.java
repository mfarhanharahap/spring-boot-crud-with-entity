package com.example.springdatajpa.repository;

import com.example.springdatajpa.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
