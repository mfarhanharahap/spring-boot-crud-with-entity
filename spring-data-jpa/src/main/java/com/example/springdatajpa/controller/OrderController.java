package com.example.springdatajpa.controller;

import com.example.springdatajpa.entity.Order;
import com.example.springdatajpa.services.OrderServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {

    @Autowired
    OrderServices orderServices;

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrder() {
        return new ResponseEntity<>(orderServices.getAllOrders(), HttpStatus.OK);
    }

    @GetMapping("/orders/{user_id}")
    public ResponseEntity<List<Order>> getOrderById(@PathVariable List<Long> user_id) {
        return new ResponseEntity<>(orderServices.getOrderByUserId(user_id), HttpStatus.OK);
    }

    @PostMapping("/orders")
    public ResponseEntity<String> addOrder(@RequestBody Order order) {
        orderServices.createOrder(order);
        return new ResponseEntity<>("Order create successfully!", HttpStatus.CREATED);
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Order> updateOrder(@RequestBody Order order, @PathVariable Long id) {
        orderServices.updateOrderById(order, id);
        return new ResponseEntity<>(order, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<String> deleteOrder(@PathVariable Long id) {
        orderServices.deleteOrderById(id);
        return new ResponseEntity<>("Order deleted successfully!", HttpStatus.ACCEPTED);
    }
}
